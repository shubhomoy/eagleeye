# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/shubhomoy/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep class !android.support.v7.internal.view.menu.**,** {*;}
-dontwarn
-ignorewarnings
-dontshrink
-dontwarn org.apache.commons.logging.impl.ServletContextCleaner
-dontwarn org.apache.commons.logging.impl.Log4JLogger
-dontwarn org.apache.commons.logging.impl.AvalonLogger
-dontwarn org.apache.commons.logging.impl.LogKitLogger

-keepnames class com.bitslate.pinpost.** {*;}

-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**
-keep class android.support.v7.** { *; }
-keep class android.support.design.**{*;}
-keep interface android.support.v7.** { *; }

-keepattributes Signature, *Annotation*
-keep public class com.google.gson
