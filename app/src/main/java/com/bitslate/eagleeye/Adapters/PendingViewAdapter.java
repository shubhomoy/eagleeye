package com.bitslate.eagleeye.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bitslate.eagleeye.Objects.Phone;
import com.bitslate.eagleeye.PendingActivity;
import com.bitslate.eagleeye.R;

import java.util.ArrayList;

/**
 * Created by ddvlslyr on 10/12/15.
 */
public class PendingViewAdapter extends RecyclerView.Adapter<PendingViewAdapter.PendingViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Phone> phones;

    public PendingViewAdapter(Context context, ArrayList<Phone> phones) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.phones = phones;
    }

    @Override
    public PendingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.item_phone, parent, false);
        PendingViewHolder viewHolder = new PendingViewHolder(row);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PendingViewHolder holder, final int position) {
        holder.textView.setText(phones.get(position).client.phone_no);
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phones.get(position).verified == 0)
                    ((PendingActivity) context).confirmRemote(position);
                else
                    ((PendingActivity) context).confirmRemove(position);
            }
        });
        if (phones.get(position).verified == 0)
            holder.icon.setImageResource(R.mipmap.ic_hourglass_full_black_24dp);
        else
            holder.icon.setImageResource(R.mipmap.ic_done_black_24dp);

    }

    @Override
    public int getItemCount() {
        return phones.size();
    }

    public class PendingViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;
        private RelativeLayout root;
        ImageView icon;

        public PendingViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView)itemView.findViewById(R.id.pending_icon);
            textView = (TextView) itemView.findViewById(R.id.phone_tv);
            root = (RelativeLayout) itemView.findViewById(R.id.item);
        }
    }
}
