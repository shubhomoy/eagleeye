package com.bitslate.eagleeye.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.eagleeye.Objects.Phone;
import com.bitslate.eagleeye.R;
import com.bitslate.eagleeye.RemoteNumberActivity;
import com.bitslate.eagleeye.Utilities.Config;
import com.bitslate.eagleeye.Utilities.Helper;
import com.bitslate.eagleeye.Utilities.VolleySingleton;
import com.bitslate.eagleeye.ViewImageActivity;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shubhomoy on 10/12/15.
 */
public class PhoneListAdapter extends RecyclerView.Adapter<PhoneListAdapter.PhoneListHolder> {

    Context context;
    ArrayList<Phone> list;
    AlertDialog.Builder alertDialog;
    ProgressDialog progressDialog;

    public PhoneListAdapter(Context context, ArrayList<Phone> list) {
        this.context = context;
        this.list = list;
        alertDialog = new AlertDialog.Builder(context);
        progressDialog = new ProgressDialog(context);
    }

    @Override
    public PhoneListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_remotenum_list, parent, false);
        PhoneListHolder holder = new PhoneListHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(PhoneListHolder holder, final int position) {
        final Phone phone = list.get(position);
        holder.phoneTv.setText(phone.remote.phone_no);
        if(phone.name !=null)
            holder.nameTv.setText(phone.name);
        else
            holder.nameTv.setText("Give a name");
        holder.nameTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditDialog(phone);
            }
        });
        holder.capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage(phone);
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog(phone);
            }
        });
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewImageActivity.class);
                intent.putExtra("remote_id", phone.remote_id);
                intent.putExtra("remote_phone_no",phone.remote.phone_no);
                intent.putExtra("remote_name",phone.name);
                context.startActivity(intent);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.setTitle("Remove");
                alertDialog.setMessage("Are you sure you want to remove this remote device?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        removeDevice(phone, position);
                    }
                });
                alertDialog.setNegativeButton("No", null);
                alertDialog.create().show();
            }
        });


        if (phone.verified == 0)
            holder.icon.setImageResource(R.mipmap.ic_hourglass_full_black_18dp);
        else
            holder.icon.setImageResource(R.mipmap.ic_done_black_18dp);
    }

    void showEditDialog(final Phone phone) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Edit");
        View view = LayoutInflater.from(context).inflate(R.layout.edit_phone_view, null);
        final EditText nameEt = (EditText)view.findViewById(R.id.nameEt);
        builder.setView(view);
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(nameEt.getText().toString().trim().length()>0) {
                    progressDialog.setTitle("Updating");
                    progressDialog.setMessage("Please wait");
                    String url = Config.URL + "/eagle/update/phone";
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Gson gson = new Gson();
                                Phone responsePhone = gson.fromJson(jsonObject.getString("data"), Phone.class);
                                phone.name = responsePhone.name;
                                PhoneListAdapter.this.notifyDataSetChanged();
                            } catch (JSONException e) {
                                Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                            }
                            progressDialog.dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("option", error.toString());
                            Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String, String> params = new HashMap<String, String>();
                            params.put("id", String.valueOf(Helper.getId(context)));
                            params.put("access_token", Helper.getAccessToken(context));
                            params.put("name", nameEt.getText().toString());
                            params.put("remote_id", String.valueOf(phone.remote_id));
                            params.put("code", phone.code);
                            return params;
                        }
                    };
                    VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    void removeDevice(final Phone phone, final int position) {
        progressDialog.setTitle("Removing");
        progressDialog.setMessage("Please wait");
        String url = Config.URL + "/eagle/remove";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                list.remove(position);
                ((RemoteNumberActivity) context).setEmptyView(list);
                PhoneListAdapter.this.notifyDataSetChanged();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                progressDialog.dismiss();
                Toast.makeText(context, "Connection Timeout. Could not remove device", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(Helper.getId(context)));
                params.put("access_token", Helper.getAccessToken(context));
                params.put("code", phone.code);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PhoneListHolder extends RecyclerView.ViewHolder {

        TextView nameTv, phoneTv;
        RelativeLayout item;
        ImageView edit, capture, delete, view, icon;

        public PhoneListHolder(View itemView) {
            super(itemView);
            phoneTv = (TextView) itemView.findViewById(R.id.number);
            nameTv = (TextView) itemView.findViewById(R.id.name);
            item = (RelativeLayout) itemView.findViewById(R.id.item);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            capture = (ImageView) itemView.findViewById(R.id.capture);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            view = (ImageView) itemView.findViewById(R.id.view_captured);
        }
    }

    private void captureImage(final Phone phone) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Capture");
        builder.setMessage("Send capture command?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String url = Config.URL + "/capture";
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(context, "Capture command sent. Please wait", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("option", error.toString());
                        Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("id", String.valueOf(Helper.getId(context)));
                        params.put("access_token", Helper.getAccessToken(context));
                        params.put("remote_id", String.valueOf(phone.remote_id));
                        params.put("code", phone.code);
                        return params;
                    }
                };
                VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }
}
