package com.bitslate.eagleeye;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.eagleeye.Objects.Phone;
import com.bitslate.eagleeye.Utilities.Config;
import com.bitslate.eagleeye.Utilities.Helper;
import com.bitslate.eagleeye.Utilities.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shubhomoy on 8/12/15.
 */
public class ConfirmRemoteActivity extends AppCompatActivity {

    Intent intent;
    int client_id, remote_id;
    ProgressDialog progressDialog;

    void initialize() {
        progressDialog = new ProgressDialog(this);
        intent = getIntent();
        client_id = intent.getIntExtra("client_id", 0);
        remote_id = intent.getIntExtra("remote_id", 0);
        //verifyFromServerAndCapture();
    }

//    void sendCodeToServer() {
//        progressDialog.setMessage("Please wait");
//        progressDialog.show();
//        progressDialog.setCancelable(false);
//        String url = Config.URL + "/auth/initialize/" + code;
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                progressDialog.dismiss();
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("msg").equals("valid")) {
//                        showSuccessDialog();
//                    } else {
//                        showNegativeDialog();
//                    }
//                } catch (JSONException e) {
//                    Toast.makeText(ConfirmRemoteActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Toast.makeText(ConfirmRemoteActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
//                Log.d("option", error.toString());
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("client_no", sender_no);
//                params.put("remote_no", Helper.getFromPref(ConfirmRemoteActivity.this, Helper.NUMBER));
//                return params;
//            }
//        };
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
//    }

    void showSuccessDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remote Verified");
        builder.setCancelable(false);
        builder.setMessage("This device is verified to act as a remote camera.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.create().show();
    }

    void showNegativeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Failed");
        builder.setMessage("Verification failed.");
        builder.setCancelable(false);
        builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.create().show();
    }

//    void verifyFromServerAndCapture() {
//        String url = Config.URL + "/capture";
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("msg").equals("valid")) {
//                        Gson gson = new Gson();
//                        Phone phone = gson.fromJson(jsonObject.getString("data"), Phone.class);
//                        Intent intent = new Intent(ConfirmRemoteActivity.this, CameraActivity.class);
//                        intent.putExtra("client_no", phone.client.phone_no);
//                        intent.putExtra("remote_id", phone.remote_id);
//                        intent.putExtra("client_id", phone.client_id);
//                        startActivity(intent);
//                        finish();
//                    } else
//                        finish();
//                } catch (JSONException e) {
//
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("option", error.toString());
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<>();
//                params.put("id", String.valueOf(Helper.getId(ConfirmRemoteActivity.this)));
//                params.put("access_token", Helper.getAccessToken(ConfirmRemoteActivity.this));
//                params.put("client_no", sender_no);
//                return params;
//            }
//        };
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_remote);
        initialize();
    }
}
