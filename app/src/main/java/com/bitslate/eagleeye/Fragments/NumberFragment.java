package com.bitslate.eagleeye.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.eagleeye.Objects.User;
import com.bitslate.eagleeye.R;
import com.bitslate.eagleeye.RemoteNumberActivity;
import com.bitslate.eagleeye.Utilities.Config;
import com.bitslate.eagleeye.Utilities.EagleEyeGCM;
import com.bitslate.eagleeye.Utilities.Helper;
import com.bitslate.eagleeye.Utilities.VolleySingleton;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class NumberFragment extends Fragment {

    private EditText number;
    private ImageView regNum;
    Button proceed;
    ProgressDialog progressDialog;

    public NumberFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_number, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        number = (EditText) getActivity().findViewById(R.id.number);
        regNum = (ImageView) getActivity().findViewById(R.id.reg_number);
        proceed = (Button)getActivity().findViewById(R.id.proceed);

        regNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRegNum();
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRegNum();
            }
        });

        number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() >= 10) {
                    regNum.setVisibility(View.VISIBLE);
                    proceed.setVisibility(View.VISIBLE);
                }else {
                    regNum.setVisibility(View.GONE);
                    proceed.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getActivity());
    }

    private void setRegNum() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(Html.fromHtml("Verify your number:" + "<br/><br/><b>" + number.getText() + "</b>"));
        builder.setTitle("Verify");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(number.getText().toString().trim().length()>0) {
                    login("+91" + number.getText().toString());
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    void login(final String client_no) {
        progressDialog.setTitle("Registering");
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        String url = Config.URL + "/eagle/init";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Gson gson = new Gson();
                    User user = gson.fromJson(jsonObject.getString("data"), User.class);
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(client_no, null, "EagleEye OTP " + user.otp, null, null);
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                    builder.setTitle("Enter OTP");
                    View v = LayoutInflater.from(getActivity()).inflate(R.layout.custom_remote_num_dialog, null);
                    final EditText otpEt = (EditText)v.findViewById(R.id.remote_num);
                    otpEt.setHint("OTP");
                    builder.setView(v);
                    builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (otpEt.getText().toString().trim().length()>0) {
                                sendOtp(client_no, otpEt.getText().toString());
                            }
                        }
                    });
                    builder.setCancelable(false);
                    builder.create().show();
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                Toast.makeText(getActivity(), "Connection Timeout", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("phone_no", client_no);
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    void sendOtp(final String client_no, final String otp) {
        progressDialog.setTitle("Verifying");
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        String url = Config.URL + "/eagle/login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if(jsonObject.getString("msg").equals("valid")) {
                        Gson gson = new Gson();
                        User user = gson.fromJson(jsonObject.getString("data"), User.class);
                        Helper.saveToPref(getActivity(), Helper.ID, user.id);
                        Helper.saveToPref(getActivity(), Helper.NUMBER, user.phone_no);
                        Helper.saveToPref(getActivity(), Helper.ACCESSTOKEN, jsonObject.getString("access_token"));
                        new Register().execute();
                    }else{
                        Toast.makeText(getActivity(), "Incorrect OTP", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                Toast.makeText(getActivity(), "Connection Timeout", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params= new HashMap<>();
                params.put("phone_no", client_no);
                params.put("otp", otp);
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    public class Register extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
            progressDialog.setTitle("Almost there");
            progressDialog.setMessage("Please wait");
        }

        @Override
        protected String doInBackground(Void... voids) {
            String msg = "";

            try {
                if (EagleEyeGCM.gcm == null) {
                    EagleEyeGCM.gcm = GoogleCloudMessaging.getInstance(getActivity());
                }
                EagleEyeGCM.regid = EagleEyeGCM.gcm.register(EagleEyeGCM.SENDER_ID);
                msg = "1";
                SharedPreferences prefs = getActivity().getSharedPreferences("Eagleeye", Context.MODE_PRIVATE);
                int appVersion = EagleEyeGCM.getAppVersion(getActivity());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(EagleEyeGCM.PROPERTY_REG_ID, EagleEyeGCM.regid);
                editor.putInt("app_version", appVersion);
                editor.commit();
            } catch (IOException ex) {
                msg = "0";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.isEmpty()) {
                if (progressDialog.isShowing()) progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Slow connection");
                builder.setCancelable(false);
                builder.setMessage("Registration was incomplete. Try again?");
                builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new Register().execute();
                    }
                });
                builder.create().show();
            } else if (s.equals("1")) {
                completeRegistration();
            }
        }
    }

    void completeRegistration() {
        String url = Config.URL+"/eagle/insertGcm";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Helper.setLoggedIn(getActivity(), true);
                startActivity(new Intent(getActivity(), RemoteNumberActivity.class));
                getActivity().finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                builder.setTitle("Retry?");
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        completeRegistration();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("id", String.valueOf(Helper.getId(getActivity())));
                params.put("access_token", Helper.getAccessToken(getActivity()));
                params.put("gcm_id", Helper.getGcmId(getActivity()));
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }
}
