package com.bitslate.eagleeye;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.eagleeye.Adapters.IntroViewPagerAdapter;
import com.bitslate.eagleeye.Fragments.Intro1;
import com.bitslate.eagleeye.Fragments.Intro2;
import com.bitslate.eagleeye.Fragments.Intro3;
import com.bitslate.eagleeye.Fragments.NumberFragment;
import com.bitslate.eagleeye.Utilities.Config;
import com.bitslate.eagleeye.Utilities.EagleEyeGCM;
import com.bitslate.eagleeye.Utilities.Helper;
import com.bitslate.eagleeye.Utilities.VolleySingleton;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class IntroActivity extends AppCompatActivity {

    private ViewPager introPager;
    private ArrayList<Fragment> fragments;
    TextView introText;
    Button next, prev;
    int pos = 0;
    String old_gcm;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        instantiate();
        introPager.setAdapter(new IntroViewPagerAdapter(getSupportFragmentManager(), fragments));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pos < 3) {
                    next.setVisibility(View.VISIBLE);
                    introPager.setCurrentItem(++pos, true);
                }else{
                    next.setVisibility(View.GONE);
                }
            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pos>0) {
                    prev.setVisibility(View.VISIBLE);
                    introPager.setCurrentItem(--pos, true);
                }else{
                    prev.setVisibility(View.GONE);
                }
            }
        });
        introPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        prev.setVisibility(View.GONE);
                        pos = position;
                        introText.setText("Want to get a peek of your home when you're out?");
                        break;
                    case 1:
                        prev.setVisibility(View.VISIBLE);
                        pos = position;
                        introText.setText("Capture a shot remotely with a simple command");
                        break;
                    case 2:
                        next.setVisibility(View.VISIBLE);
                        pos = position;
                        introText.setText("Access any device's camera remotely with Eagle Eye");
                        break;
                    case 3:
                        next.setVisibility(View.GONE);
                        pos = position;
                        introText.setText("");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void instantiate() {
        introText = (TextView)findViewById(R.id.intro_text);
        introText.setText("Want to get a peek of your home when you're out?");
        next = (Button)findViewById(R.id.next);
        prev = (Button)findViewById(R.id.prev);
        prev.setVisibility(View.GONE);
        progressDialog = new ProgressDialog(this);
        EagleEyeGCM eagleEyeGCM = new EagleEyeGCM(this);
        if (eagleEyeGCM.checkPlayServices()) {
            old_gcm = Helper.getGcmId(this);
            eagleEyeGCM.gcm = GoogleCloudMessaging.getInstance(this);
            eagleEyeGCM.regid = eagleEyeGCM.getRegistrationId(this);
            if (eagleEyeGCM.regid.isEmpty() && Helper.isLoggedIn(this)) {
                //old user but need to update GCM
                new UpdateGCM().execute();
            } else if (Helper.isLoggedIn(this)) {
                startActivity(new Intent(this, RemoteNumberActivity.class));
                finish();
            }
        }
        introPager = (ViewPager) findViewById(R.id.intro_pager);
        fragments = new ArrayList<>();
        fragments.add(new Intro1());
        fragments.add(new Intro2());
        fragments.add(new Intro3());
        fragments.add(new NumberFragment());
    }

    public class UpdateGCM extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(IntroActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle("Eagle Eye Update");
            progressDialog.setMessage("Please wait");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";

            try {
                if (EagleEyeGCM.gcm == null) {
                    EagleEyeGCM.gcm = GoogleCloudMessaging.getInstance(IntroActivity.this);
                }
                EagleEyeGCM.regid = EagleEyeGCM.gcm.register(EagleEyeGCM.SENDER_ID);
                msg = "1";
                SharedPreferences prefs = getSharedPreferences("Eagleeye", Context.MODE_PRIVATE);
                int appVersion = EagleEyeGCM.getAppVersion(IntroActivity.this);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(EagleEyeGCM.PROPERTY_REG_ID, EagleEyeGCM.regid);
                editor.putInt("app_version", appVersion);
                editor.commit();
            } catch (IOException ex) {
                msg = "0";
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s != null && s.equals("0")) {
                if (progressDialog.isShowing()) progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(IntroActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Connection Slow");
                builder.setMessage("Your connection seems slow. Try again");
                builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new UpdateGCM().execute();
                    }
                });
                builder.create().show();
                Intent intent = new Intent(IntroActivity.this, RemoteNumberActivity.class);
                startActivity(intent);
                finish();
            } else if(s.equals("1")){
                completeRegistration();
            }
        }
    }

    void completeRegistration() {
        progressDialog.show();
        progressDialog.setTitle("Configuring");
        progressDialog.setMessage("Please wait");
        String url = Config.URL + "/eagle/updateGCM";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Intent intent = new Intent(IntroActivity.this, RemoteNumberActivity.class);
                startActivity(intent);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing()) progressDialog.dismiss();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(IntroActivity.this);
                alertDialog.setTitle("Slow connection");
                alertDialog.setMessage("Your connection seems slow.\nTry again?");
                alertDialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        completeRegistration();
                    }
                });
                alertDialog.setCancelable(false);
                alertDialog.create().show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(Helper.getId(IntroActivity.this)));
                params.put("new_reg_id", Helper.getGcmId(IntroActivity.this));
                params.put("old_reg_id", old_gcm);
                return params;
            }
        };
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }
}