package com.bitslate.eagleeye.Objects;

/**
 * Created by shubhomoy on 10/12/15.
 */
public class Phone {
    public int id;
    public int remote_id;
    public int client_id;
    public String code;
    public String name;
    public int verified;
    public User remote;
    public User client;
}
