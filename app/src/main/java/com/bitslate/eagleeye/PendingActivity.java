package com.bitslate.eagleeye;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.eagleeye.Adapters.PendingViewAdapter;
import com.bitslate.eagleeye.Objects.Phone;
import com.bitslate.eagleeye.Objects.User;
import com.bitslate.eagleeye.Utilities.Config;
import com.bitslate.eagleeye.Utilities.Helper;
import com.bitslate.eagleeye.Utilities.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PendingActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Toolbar toolbar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CardView emptyText;
    private RelativeLayout root;
    private RecyclerView pendingView;
    private ArrayList<Phone> phones;
    private PendingViewAdapter adapter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending);

        instantiate();
        adapter = new PendingViewAdapter(this, phones);
        pendingView.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(this);
        getPendingNumbers();
    }

    private void instantiate() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pending Requests");
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        root = (RelativeLayout) findViewById(R.id.root);
        emptyText = (CardView) findViewById(R.id.empty_pending_text);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);

        phones = new ArrayList<>();
        pendingView = (RecyclerView) findViewById(R.id.pendig_view);
        pendingView.setHasFixedSize(true);
        pendingView.setLayoutManager(new LinearLayoutManager(this));
        initiateRefresh();
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
    }


    private void getPendingNumbers() {
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setEnabled(false);
        phones.clear();
        phones.removeAll(phones);
        String url = Config.URL + "/eagle/requests?id=" + Helper.getId(this) + "&access_token=" + Helper.getAccessToken(this);

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("option", response.toString());
                    phones.removeAll(phones);
                    phones.clear();
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setEnabled(true);
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getString("data"), User.class);
                    for (int i = 0; i < user.clients.size(); i++) {
                        Phone phone = user.clients.get(i);
                        phones.add(phone);
                    }
                    setEmptyView(phones);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Snackbar snackbar = Snackbar.make(root, "Connection Timeout", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                swipeRefreshLayout.setRefreshing(true);
                                swipeRefreshLayout.setEnabled(false);
                                getPendingNumbers();
                            }
                        });
                snackbar.show();
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

    public void confirmRemote(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm");
        builder.setMessage("Confirm Request?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendCodeToServer(position);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                progressDialog.setMessage("Deleting the number");
                progressDialog.setCancelable(false);
                progressDialog.show();
                removeDevice(phones.get(position), position);
            }
        });
        builder.create().show();
    }

    public void confirmRemove(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remove");
        builder.setMessage("Remove this request?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressDialog.setMessage("Deleting the number");
                progressDialog.setCancelable(false);
                progressDialog.show();
                removeDevice(phones.get(position), position);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }


    void sendCodeToServer(final int position) {
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        progressDialog.setCancelable(false);
        String url = Config.URL + "/eagle/verify";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("msg").equals("valid")) {
                        Gson gson = new Gson();
                        Phone phone = gson.fromJson(jsonObject.getString("data"), Phone.class);
                        showSuccessDialog(position);
                    } else {
                        showNegativeDialog();
                    }
                } catch (JSONException e) {
                    Toast.makeText(PendingActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Snackbar snackbar = Snackbar.make(root, "Something went wrong", Snackbar.LENGTH_LONG)
                        .setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sendCodeToServer(position);
                            }
                        });
                snackbar.show();
                Log.d("option", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("client_id", String.valueOf(phones.get(position).client_id));
                params.put("code", phones.get(position).code);
                params.put("id", String.valueOf(Helper.getId(PendingActivity.this)));
                params.put("access_token", Helper.getAccessToken(PendingActivity.this));
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    void showSuccessDialog(int position) {
        phones.get(position).verified = 1;
        adapter.notifyDataSetChanged();
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Remote Verified");
        builder.setMessage("This device is verified to act as a remote camera.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    void showNegativeDialog() {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle("Failed");
        builder.setMessage("Verification failed.");
        builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    void removeDevice(final Phone phone, final int position) {
        String url = Config.URL + "/eagle/remove";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                phones.remove(position);
                setEmptyView(phones);
                adapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("option", error.toString());
                progressDialog.dismiss();
                Toast.makeText(PendingActivity.this, "Connection Timeout", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(Helper.getId(PendingActivity.this)));
                params.put("access_token", Helper.getAccessToken(PendingActivity.this));
                params.put("code", phone.code);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().getRequestQueue().add(stringRequest);
    }

    public void setEmptyView(ArrayList<Phone> phones) {

        if (phones.size() == 0) {
            emptyText.setVisibility(View.VISIBLE);
        } else {
            emptyText.setVisibility(View.GONE);
        }
    }

    private boolean isNetorkConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        return manager.getActiveNetworkInfo() != null;
    }

    private void initiateRefresh() {

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void onRefresh() {
        getPendingNumbers();
    }
}