package com.bitslate.eagleeye;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bitslate.eagleeye.Adapters.PhoneListAdapter;
import com.bitslate.eagleeye.Objects.Phone;
import com.bitslate.eagleeye.Objects.User;
import com.bitslate.eagleeye.Utilities.Config;
import com.bitslate.eagleeye.Utilities.Helper;
import com.bitslate.eagleeye.Utilities.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RemoteNumberActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private Button addRemoteNum;
    private Context context;
    private String RemoteNumber;
    private SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Phone> list;
    Toolbar toolbar;
    PhoneListAdapter adapter;
    LinearLayout emptyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote_number);

        instantiate();
        addRemoteNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupRemote();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.remote_num_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.notifications:
                startActivity(new Intent(context, PendingActivity.class));
                break;
            case R.id.help:
                startActivity(new Intent(context, HelpActivity.class));
                break;
            case R.id.about:
                startActivity(new Intent(context, AboutActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void instantiate() {
        context = RemoteNumberActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Remote Devices");
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        addRemoteNum = (Button) findViewById(R.id.remote_add);
        emptyList = (LinearLayout) findViewById(R.id.list_empty);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        list = new ArrayList<Phone>();
        adapter = new PhoneListAdapter(this, list);
        recyclerView.setAdapter(adapter);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Remote Devices");
        setEmptyView(list);
        adapter.notifyDataSetChanged();
        initiateRefresh();
        fetchPhones();
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
    }

    private void setupRemote() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.custom_remote_num_dialog, null);
        builder.setView(row);
        builder.setTitle("Remote Number");
        final EditText remote_num = (EditText) row.findViewById(R.id.remote_num);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RemoteNumber = remote_num.getText().toString();
                confirmNumber();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    private void confirmNumber() {
        RemoteNumber = "+91" + RemoteNumber;
        if (RemoteNumber.equals(Helper.getNumber(context))) {
            Toast.makeText(context, "You cannot add your number", Toast.LENGTH_LONG).show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(Html.fromHtml("Verify remote number:" + "<br/><br/><b>" + RemoteNumber + "</b>"));
            builder.setTitle("Verify");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    authRemoteNum();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.create().show();
        }
    }

    private void authRemoteNum() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Adding remote device");
        progressDialog.show();
        String url = Config.URL + "/eagle/add";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("msg").equals("already_added")) {
                        Toast.makeText(context, "Number already added", Toast.LENGTH_LONG).show();
                    } else if (object.getString("msg").equals("no_user")) {
                        Toast.makeText(context, "No user", Toast.LENGTH_LONG).show();
                    } else if (object.getString("msg").equals("valid")) {
                        Gson gson = new Gson();
                        Phone phone = gson.fromJson(object.getString("data"), Phone.class);
                        list.add(phone);
                        setEmptyView(list);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    Toast.makeText(context, "Connection Timeout", Toast.LENGTH_LONG).show();
                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("options", error.toString());
                Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> data = new HashMap<>();
                data.put("id", String.valueOf(Helper.getId(context)));
                data.put("access_token", Helper.getAccessToken(context));
                data.put("remote_no", RemoteNumber);
                return data;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }

    void fetchPhones() {
        String url = Config.URL + "/eagle/all?id=" + Helper.getId(context) + "&access_token=" + Helper.getAccessToken(context);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setEnabled(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
                try {
                    list.removeAll(list);
                    list.clear();
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(response.getString("data"));
                    Gson gson = new Gson();
                    for(int i=0; i<jsonArray.length(); i++) {
                        Phone phone = gson.fromJson(jsonArray.getJSONObject(i).toString(), Phone.class);
                        list.add(phone);
                    }
                    setEmptyView(list);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);
                Log.d("option", error.toString());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance().getRequestQueue().add(jsonObjectRequest);
    }


    public void setEmptyView(ArrayList<Phone> list) {
        if (list.size() == 0) {
            emptyList.setVisibility(View.VISIBLE);
        } else {
            emptyList.setVisibility(View.GONE);
        }
    }

    private void initiateRefresh() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void onRefresh() {
        fetchPhones();
    }
}