package com.bitslate.eagleeye.Services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.bitslate.eagleeye.CameraActivity;
import com.bitslate.eagleeye.ConfirmRemoteActivity;
import com.bitslate.eagleeye.PendingActivity;
import com.bitslate.eagleeye.Receivers.GCMBroadcastReceiver;
import com.bitslate.eagleeye.RemoteNumberActivity;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by shubhomoy on 15/12/15.
 */
public class GcmIntentService extends IntentService {

    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    public GcmIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (extras != null) {
            if (!extras.isEmpty()) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                    //sendNotification("Send error: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                    //sendNotification("Deleted messages on server: " +extras.toString(),0);
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                    if (extras.get("action").equals("request"))
                        sendNotification(extras.getString("message"), extras.getString("action"));
                    else if (extras.get("action").equals("capture")) {
                        sendNotification(Integer.parseInt(extras.getString("client_id")), Integer.parseInt(extras.getString("remote_id")));
                    }else if (extras.get("action").equals("capture_confirm")){
                        sendNotification(extras.getString("message"), extras.getString("action"));
                    }
                }
            }
        }
        GCMBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String msg, String action) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (action.equals("request")) {
            Intent i = new Intent(this, PendingActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(android.R.drawable.stat_notify_chat)
                            .setContentTitle("Eagle Eye")
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(msg))
                            .setLights(Color.WHITE, 2000, 2000)
                            .setVibrate(new long[]{0, 300, 300, 300})
                            .setContentText(msg);
            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(1, mBuilder.build());
        }else if(action.equals("capture_confirm")) {
            Intent i = new Intent(this, RemoteNumberActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(android.R.drawable.stat_notify_chat)
                            .setContentTitle("Eagle Eye")
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .bigText(msg))
                            .setLights(Color.WHITE, 2000, 2000)
                            .setVibrate(new long[]{0, 300, 300, 300})
                            .setContentText(msg);
            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(1, mBuilder.build());
        }
    }

    private void sendNotification(int client_id, int remote_id) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent in = new Intent(this, CameraActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        in.putExtra("client_id", client_id);
        in.putExtra("remote_id", remote_id);
        startActivity(in);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, in, PendingIntent.FLAG_UPDATE_CURRENT);
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(android.R.drawable.stat_notify_chat)
//                        .setContentTitle("Eagle Eye")
//                        .setStyle(new NotificationCompat.BigTextStyle()
//                                .bigText("Capture request"))
//                        .setContentText("Capture request");
//        mBuilder.setContentIntent(contentIntent);
//        mNotificationManager.notify(1, mBuilder.build());
    }
}
