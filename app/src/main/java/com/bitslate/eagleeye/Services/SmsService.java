package com.bitslate.eagleeye.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bitslate.eagleeye.ConfirmRemoteActivity;
import com.bitslate.eagleeye.RemoteNumberActivity;

/**
 * Created by shubhomoy on 8/12/15.
 */
public class SmsService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent dialogIntent = new Intent(this, ConfirmRemoteActivity.class);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        dialogIntent.putExtra("action", intent.getStringExtra("action"));
        if(intent.getStringExtra("action").equals("verify")) {
            dialogIntent.putExtra("code", intent.getStringExtra("code"));
        }
        dialogIntent.putExtra("sender_no", intent.getStringExtra("sender_no"));
        Log.d("option", "from service: "+intent.getStringExtra("sender_no"));
        startActivity(dialogIntent);
        stopService(new Intent(getApplicationContext(), SmsService.class));
        return super.onStartCommand(intent, flags, startId);
    }
}
