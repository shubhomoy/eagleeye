package com.bitslate.eagleeye.Utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import com.bitslate.eagleeye.Objects.Phone;

import java.util.ArrayList;

/**
 * Created by ddvlslyr on 8/12/15.
 */
public class Helper {

    private static final String FILENAME = "Eagleeye";
    public static final String NUMBER = "Number";
    public static final String ACCESSTOKEN = "AccessToken";
    public static final String ID = "id";

    public static void saveToPref(Context context, String key, String val) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public static void saveToPref(Context context, String key, int val) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, val);
        editor.commit();
    }

    public static String getFromPref(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        return preferences.getString(key, null);
    }

    public static int getId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        return preferences.getInt(ID, 0);
    }

    public static String getAccessToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        return preferences.getString(ACCESSTOKEN, null);
    }

    public static String getNumber(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        return preferences.getString(NUMBER, null);
    }

    public static String getGcmId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        return preferences.getString(EagleEyeGCM.PROPERTY_REG_ID, "");
    }

    public static void setLoggedIn(Context context, boolean val) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("logged_in", val);
        editor.commit();
    }

    public static boolean isLoggedIn(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME, context.MODE_PRIVATE);
        return preferences.getBoolean("logged_in", false);
    }
}
