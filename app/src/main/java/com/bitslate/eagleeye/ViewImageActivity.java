package com.bitslate.eagleeye;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bitslate.eagleeye.Utilities.Config;
import com.bitslate.eagleeye.Utilities.Helper;
import com.bumptech.glide.Glide;

import java.util.Date;

public class ViewImageActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView captured;
    private TextView number,name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

        instantate();

        Intent intent=getIntent();
        int remote_id = intent.getIntExtra("remote_id", 0);
        String  phone_num=intent.getStringExtra("remote_phone_no");
        loadPicture(remote_id);
        name.setText(intent.getStringExtra("remote_name"));
        number.setText(phone_num);
    }

    void instantate() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        captured = (ImageView) findViewById(R.id.image);
        number= (TextView) findViewById(R.id.number);
        name = (TextView)findViewById(R.id.name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void loadPicture(int remote_id) {
        String url = Config.URL + "/file/" + remote_id + "?id=" + Helper.getId(this) + "&access_token=" + Helper.getAccessToken(this)+"&t="+System.currentTimeMillis();
        Glide.with(this).load(url).into(captured);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
